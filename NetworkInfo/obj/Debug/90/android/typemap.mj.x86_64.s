	/* Data SHA1: 4b6b80304239fe2ec955bba74d3242283686a131 */
	.file	"typemap.mj.inc"

	/* Mapping header */
	.section	.data.mj_typemap,"aw",@progbits
	.type	mj_typemap_header, @object
	.p2align	2
	.global	mj_typemap_header
mj_typemap_header:
	/* version */
	.long	1
	/* entry-count */
	.long	254
	/* entry-length */
	.long	216
	/* value-offset */
	.long	122
	.size	mj_typemap_header, 16

	/* Mapping data */
	.type	mj_typemap, @object
	.global	mj_typemap
mj_typemap:
	.size	mj_typemap, 54865
	.include	"typemap.mj.inc"
