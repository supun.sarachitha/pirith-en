package md52a35be5f26062a6ecb6235cc4c30b366;


public class MainActivity_OnClickListener
	extends java.lang.Object
	implements
		mono.android.IGCUserPeer,
		android.view.View.OnClickListener
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onClick:(Landroid/view/View;)V:GetOnClick_Landroid_view_View_Handler:Android.Views.View/IOnClickListenerInvoker, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null\n" +
			"";
		mono.android.Runtime.register ("Pirith.MainActivity+OnClickListener, Pirith", MainActivity_OnClickListener.class, __md_methods);
	}


	public MainActivity_OnClickListener ()
	{
		super ();
		if (getClass () == MainActivity_OnClickListener.class)
			mono.android.TypeManager.Activate ("Pirith.MainActivity+OnClickListener, Pirith", "", this, new java.lang.Object[] {  });
	}

	public MainActivity_OnClickListener (md52a35be5f26062a6ecb6235cc4c30b366.MainActivity p0)
	{
		super ();
		if (getClass () == MainActivity_OnClickListener.class)
			mono.android.TypeManager.Activate ("Pirith.MainActivity+OnClickListener, Pirith", "Pirith.MainActivity, Pirith", this, new java.lang.Object[] { p0 });
	}


	public void onClick (android.view.View p0)
	{
		n_onClick (p0);
	}

	private native void n_onClick (android.view.View p0);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
