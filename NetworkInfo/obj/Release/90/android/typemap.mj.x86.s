	/* Data SHA1: 8fb799c914ebd93cdf8b6095e74bf06a94e61e8d */
	.file	"typemap.mj.inc"

	/* Mapping header */
	.section	.data.mj_typemap,"aw",@progbits
	.type	mj_typemap_header, @object
	.p2align	2
	.global	mj_typemap_header
mj_typemap_header:
	/* version */
	.long	1
	/* entry-count */
	.long	534
	/* entry-length */
	.long	216
	/* value-offset */
	.long	122
	.size	mj_typemap_header, 16

	/* Mapping data */
	.type	mj_typemap, @object
	.global	mj_typemap
mj_typemap:
	.size	mj_typemap, 115345
	.include	"typemap.mj.inc"
