	/* Data SHA1: 6f47aa5a7a9316a202f13cbfbaa95fa6b6e22391 */
	.file	"typemap.jm.inc"

	/* Mapping header */
	.section	.data.jm_typemap,"aw",@progbits
	.type	jm_typemap_header, @object
	.p2align	2
	.global	jm_typemap_header
jm_typemap_header:
	/* version */
	.long	1
	/* entry-count */
	.long	467
	/* entry-length */
	.long	216
	/* value-offset */
	.long	94
	.size	jm_typemap_header, 16

	/* Mapping data */
	.type	jm_typemap, @object
	.global	jm_typemap
jm_typemap:
	.size	jm_typemap, 100873
	.include	"typemap.jm.inc"
