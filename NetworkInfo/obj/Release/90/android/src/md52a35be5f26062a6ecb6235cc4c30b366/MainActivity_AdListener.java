package md52a35be5f26062a6ecb6235cc4c30b366;


public class MainActivity_AdListener
	extends com.google.android.gms.ads.AdListener
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"";
		mono.android.Runtime.register ("Pirith.MainActivity+AdListener, Pirith", MainActivity_AdListener.class, __md_methods);
	}


	public MainActivity_AdListener ()
	{
		super ();
		if (getClass () == MainActivity_AdListener.class)
			mono.android.TypeManager.Activate ("Pirith.MainActivity+AdListener, Pirith", "", this, new java.lang.Object[] {  });
	}

	public MainActivity_AdListener (md52a35be5f26062a6ecb6235cc4c30b366.MainActivity p0)
	{
		super ();
		if (getClass () == MainActivity_AdListener.class)
			mono.android.TypeManager.Activate ("Pirith.MainActivity+AdListener, Pirith", "Pirith.MainActivity, Pirith", this, new java.lang.Object[] { p0 });
	}

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
