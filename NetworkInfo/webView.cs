﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Webkit;
using Android.Util;
using Android.Support.V7.App;

namespace Pirith
{

    [Activity(Label = "webView", Theme = "@style/AppTheme.NoActionBar" , MainLauncher = false)]
    public class webView : AppCompatActivity
    {
        WebView web_view;
        protected static View loadingSpinner;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.WebViewLayout);


            Android.Support.V7.Widget.Toolbar toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);

            
            web_view = FindViewById<WebView>(Resource.Id.webview);
            web_view.Settings.JavaScriptEnabled = true;
            web_view.SetWebViewClient(new HelloWebViewClient());
            web_view.LoadUrl(Common.WebUrl);
            web_view.Settings.JavaScriptEnabled = true;
            web_view.Settings.BuiltInZoomControls = true;
            web_view.Settings.SetSupportZoom(true);
            web_view.ScrollBarStyle = ScrollbarStyles.OutsideOverlay;
            web_view.ScrollbarFadingEnabled = false;

        }


        public override bool OnKeyDown(Android.Views.Keycode keyCode, Android.Views.KeyEvent e)
        {
            if (keyCode == Keycode.Back && web_view.CanGoBack())
            {
                web_view.GoBack();
                return true;
            }
            //else if (!web_view.CanGoBack())
            //{
            //    StartActivity(typeof(MainActivity));
            //    Finish();
            //}
            return base.OnKeyDown(keyCode, e);
        }

        public override void OnBackPressed()
        {
            StartActivity(typeof(MainActivity));
            Finish();
        }


    }

    public class HelloWebViewClient : WebViewClient
    {
        // For API level 24 and later
        public override bool ShouldOverrideUrlLoading(WebView view, IWebResourceRequest request)
        {
            view.LoadUrl(request.Url.ToString());
            return false;
        }

        public override void OnPageFinished(WebView view, string url)
        {
            base.OnPageFinished(view, url);
        }
    }
}