﻿using System;
using System.Collections.Generic;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.V4.App;
using Android.Util;

using Firebase.Messaging;

namespace Pirith
{
    [Service]
    [IntentFilter(new[] {"com.google.firebase.MESSAGING_EVENT"})]
    public class MyFirebaseMessagingService : FirebaseMessagingService
    {
        const string TAG = "MyFirebaseMsgService";

        public override void OnMessageReceived(RemoteMessage message)
        {
            //Log.Debug(TAG, "From: " + message.From);

            var body = message.GetNotification().Body;
            //Log.Debug(TAG, "Notification Message Body: " + body);
            SendNotification(body, message.Data);
        }

        void SendNotification(string messageBody, IDictionary<string, string> data)
        {
            try
            {
                Intent intent = null;
                intent = new Intent(this, typeof(MainActivity));

                foreach (var key in data.Keys)
                {
                    if (key == "update")
                    {
                        try
                        {
                            intent = new Intent(Intent.ActionView, Android.Net.Uri.Parse("market://details?id=" + data[key].ToString()));
                        }
                        catch (Android.Content.ActivityNotFoundException anfe)
                        {
                            intent = new Intent(Intent.ActionView, Android.Net.Uri.Parse("http://play.google.com/store/apps/details?id=" + data[key].ToString()));

                        }
                    }
                    else
                    {
                        intent = new Intent(this, typeof(MainActivity));
                    }
                }


                intent.AddFlags(ActivityFlags.ClearTop);
                foreach (var key in data.Keys)
                {
                    intent.PutExtra(key, data[key]);
                }


                var pendingIntent = PendingIntent.GetActivity(this, MainActivity.NOTIFICATION_ID, intent, PendingIntentFlags.OneShot);

                var notificationBuilder = new NotificationCompat.Builder(this, MainActivity.CHANNEL_ID)
                                          .SetSmallIcon(Resource.Drawable.outline_message_24)
                                          .SetContentTitle(Resources.GetString(Resource.String.app_name))
                                          .SetContentText(messageBody)
                                          .SetAutoCancel(true)
                                          .SetContentIntent(pendingIntent);

                var notificationManager = NotificationManagerCompat.From(this);
                notificationManager.Notify(MainActivity.NOTIFICATION_ID, notificationBuilder.Build());
            }
            catch (Exception)
            {

                return;
            }
            
        }


    }
}
