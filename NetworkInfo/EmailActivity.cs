﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Essentials;

namespace Pirith
{
    [Activity(Label = "EmailActivity")]
    public class EmailActivity : Activity
    {

        EditText Name;
        EditText Email;
        EditText Msg;
        Button send;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.emailView);

            Name = FindViewById<EditText>(Resource.Id.Name);
            Email = FindViewById<EditText>(Resource.Id.Email);
            Msg = FindViewById<EditText>(Resource.Id.Msg);
            send = FindViewById<Button>(Resource.Id.Send);

            send.Click += Send_Click;



            // Create your application here
        }

        private void Send_Click(object sender, EventArgs e)
        {
            try
            {
                

                this.RunOnUiThread(async () =>
                {

                    try
                    {
                        Android.App.AlertDialog.Builder dialog = new AlertDialog.Builder(this);
                        AlertDialog alert = dialog.Create();
                        alert.SetTitle("");
                        alert.SetMessage("පණිවිඩය යොමු කරමින් පවතී...");
                        alert.Show();

                        await Task.Delay(1000);

                        string mailbody =
                        "name : " + Name.Text.ToString() + "\n" +
                        "Email : " +  Email.Text.ToString() + "\n" +
                        "Message : " + Msg.Text.ToString() + "\n" +
                        "App Name : " + AppInfo.Name + "\n" +
                        "App Version : " + AppInfo.VersionString + "\n" +
                        "Main Display Info : " + DeviceDisplay.MainDisplayInfo + "\n" +
                        "Model : " + DeviceInfo.Model + "\n" +
                        "Manufacturer : " + DeviceInfo.Manufacturer + "\n" +
                        "Device Name : " + DeviceInfo.Name + "\n" +
                        "OS Version : " + DeviceInfo.VersionString + "\n" +
                        "Platform : " + DeviceInfo.Platform + "\n" +
                        "Idiom : " + DeviceInfo.Idiom + "\n" +
                        "DeviceType : " + DeviceInfo.DeviceType;


                        MailMessage mail = new MailMessage();
                        SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");

                        mail.From = new MailAddress("stechbuzz@gmail.com");
                        mail.To.Add("stechbuzz@gmail.com");
                        mail.Subject = "Pirith FeedBack";
                        mail.Body = mailbody;
                        SmtpServer.UseDefaultCredentials = false;
                        SmtpServer.Port = 587;
                        SmtpServer.Credentials = new System.Net.NetworkCredential("stechbuzz@gmail.com", "A8oTMJFN33oN");
                        SmtpServer.EnableSsl = true;

                        SmtpServer.Send(mail);

                        alert.SetMessage("ඔබගේ ප්‍රතිචාරයට ස්තුතියි.");
                        alert.Show();

                        await Task.Delay(2000);

                        this.Finish();
                    }
                    catch (Exception)
                    {

                        this.Finish();
                    }


                });
            }
            catch (Exception ex)
            {
                this.Finish();
            }
        }
    }
}