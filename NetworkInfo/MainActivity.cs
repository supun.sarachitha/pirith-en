﻿using System;
using System.IO;
using System.Linq;
using Android;
using Android.App;
using Android.Content;
using Android.Gms.Ads;
using Android.Media;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V4.View;
using Android.Support.V4.Widget;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using Java.IO;
using Plugin.DownloadManager;
using Plugin.DownloadManager.Abstractions;
using Xamarin.Essentials;
using AlertDialog = Android.App.AlertDialog;

using Android.Support.V4.App;
using TaskStackBuilder = Android.Support.V4.App.TaskStackBuilder;
using Android.Text.Format;
using Android.Gms.Common;
using Firebase.Messaging;
using Firebase.Iid;
using Android.Util;
using Firebase.Analytics;

namespace Pirith
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = false, ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class MainActivity : AppCompatActivity, NavigationView.IOnNavigationItemSelectedListener
    {
        public static readonly string CHANNEL_ID = "Pirith_notification_channel";
        public static readonly int NOTIFICATION_ID = 100;
        const string TAG = "MainActivity";
        FirebaseAnalytics firebaseAnalytics;
        Common common = new Common();

        Button btn1;Button btn2;Button btn3;Button btn4;Button btn5;Button btn6;
        Button btn7;Button btn8;Button btn9; Button btn10;Button btn11;Button btn12;
        Button btn13; Button btn14;Button btn15;Button btn16; Button btn17;
        Button btn18;Button btn19;Button btn20;Button btn21;Button btn22;Button btn23;
        Button btn25; Button btn26; Button btn27; Button btn28; Button btn29;

        ImageButton btnstop;
        ImageButton btnplay;
        LinearLayout PlayerView;
        TextView Playing;

        AlertDialog.Builder alert;
        Dialog dialog;


        Button control;
        MediaPlayer _player;

        protected AdView mAdView;
        protected InterstitialAd mInterstitialAd;
        protected Button mLoadInterstitialButton;

        public string FolderPath;
        FloatingActionButton fab;

        public string selectedFile;

        IDownloadManager downloadManager;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_main);

            firebaseAnalytics = FirebaseAnalytics.GetInstance(this);
            Window.AddFlags(WindowManagerFlags.KeepScreenOn);

            Xamarin.Essentials.Platform.Init(this, savedInstanceState);

            Android.Support.V7.Widget.Toolbar toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);

            fab = FindViewById<FloatingActionButton>(Resource.Id.fab);
            fab.Click += FabOnClick;


            fab.Hide();

            DrawerLayout drawer = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            Android.Support.V7.App.ActionBarDrawerToggle toggle = new Android.Support.V7.App.ActionBarDrawerToggle(this, drawer, toolbar, Resource.String.navigation_drawer_open, Resource.String.navigation_drawer_close);
            drawer.AddDrawerListener(toggle);
            toggle.SyncState();

            NavigationView navigationView = FindViewById<NavigationView>(Resource.Id.nav_view);
            navigationView.SetNavigationItemSelectedListener(this);


            btn1 = FindViewById<Button>(Resource.Id.btn1);
            btn1.Click += Btn1_Click;

            btn2 = FindViewById<Button>(Resource.Id.btn2);
            btn2.Click += Btn2_Click;

            btn3 = FindViewById<Button>(Resource.Id.btn3);
            btn3.Click += Btn3_Click;

            btn4 = FindViewById<Button>(Resource.Id.btn4);
            btn4.Click += Btn4_Click;

            btn5 = FindViewById<Button>(Resource.Id.btn5);
            btn5.Click += Btn5_Click;

            btn6 = FindViewById<Button>(Resource.Id.btn6);
            btn6.Click += Btn6_Click;

            btn7 = FindViewById<Button>(Resource.Id.btn7);
            btn7.Click += Btn7_Click;

            btn8 = FindViewById<Button>(Resource.Id.btn8);
            btn8.Click += Btn8_Click;

            btn9 = FindViewById<Button>(Resource.Id.btn9);
            btn9.Click += Btn9_Click;

            btn10 = FindViewById<Button>(Resource.Id.btn10);
            btn10.Click += Btn10_Click;

            btn11 = FindViewById<Button>(Resource.Id.btn11);
            btn11.Click += Btn11_Click;

            btn12 = FindViewById<Button>(Resource.Id.btn12);
            btn12.Click += Btn12_Click;

            btn13 = FindViewById<Button>(Resource.Id.btn13);
            btn13.Click += Btn13_Click;

            btn14 = FindViewById<Button>(Resource.Id.btn14);
            btn14.Click += Btn14_Click;

            btn15 = FindViewById<Button>(Resource.Id.btn15);
            btn15.Click += Btn15_Click;

            btn16 = FindViewById<Button>(Resource.Id.btn16);
            btn16.Click += Btn16_Click;

            btn17 = FindViewById<Button>(Resource.Id.btn17);
            btn17.Click += Btn17_Click;

            btn18 = FindViewById<Button>(Resource.Id.btn18);
            btn18.Click += Btn18_Click;

            btn19 = FindViewById<Button>(Resource.Id.btn19);
            btn19.Click += Btn19_Click;

            btn20 = FindViewById<Button>(Resource.Id.btn20);
            btn20.Click += Btn20_Click;

            btn21 = FindViewById<Button>(Resource.Id.btn21);
            btn21.Click += Btn21_Click;

            btn22 = FindViewById<Button>(Resource.Id.btn22);
            btn22.Click += Btn22_Click;

            btn23 = FindViewById<Button>(Resource.Id.btn23);
            btn23.Click += Btn23_Click;

            btn25 = FindViewById<Button>(Resource.Id.btn25);
            btn25.Click += Btn25_Click;


            btn26 = FindViewById<Button>(Resource.Id.btn26);
            btn26.Click += Btn26_Click;

            btn27 = FindViewById<Button>(Resource.Id.btn27);
            btn27.Click += Btn27_Click;

            btn28 = FindViewById<Button>(Resource.Id.btn28);
            btn28.Click += Btn28_Click;

            btn29 = FindViewById<Button>(Resource.Id.btn29);
            btn29.Click += Btn29_Click;

            btnstop = FindViewById<ImageButton>(Resource.Id.btnstop);
            btnstop.SetBackgroundResource(Resource.Drawable.round_stop_24);
            btnstop.Click += btnstop_Click;

            btnplay = FindViewById<ImageButton>(Resource.Id.btnplay);
            btnplay.SetBackgroundResource(Resource.Drawable.baseline_play_arrow_24);
            btnplay.Click += btnplay_Click;

            PlayerView = FindViewById<LinearLayout>(Resource.Id.linearLayout2);

            PlayerView.Visibility = ViewStates.Gone;

            Playing = FindViewById<TextView>(Resource.Id.Playing);

            MobileAds.Initialize(this);

            mAdView = FindViewById<AdView>(Resource.Id.adView);
            var adRequest = new AdRequest.Builder().Build();
            mAdView.LoadAd(adRequest);


            //mInterstitialAd = new InterstitialAd(this);
            //mInterstitialAd.AdUnitId = GetString(Resource.String.test_interstitial_ad_unit_id);

            //mInterstitialAd.AdListener = new AdListener(this);


            FolderPath = ApplicationContext.GetExternalFilesDir(Android.OS.Environment.DirectoryDownloads).AbsolutePath;


            showIndicator();



            //=============firebase Begin===============//

            if (Intent.Extras != null)
            {
                foreach (var key in Intent.Extras.KeySet())
                {
                    var value = Intent.Extras.GetString(key);
                    Log.Debug(TAG, "Key: {0} Value: {1}", key, value);
                }
            }

            CreateNotificationChannel();

            IsPlayServicesAvailable();

            

            //FirebaseMessaging.Instance.SubscribeToTopic("PirithTestNotification");

            //login event push
            Bundle bundle = new Bundle();
            bundle.PutString("app_open", "app_open");
            firebaseAnalytics.LogEvent(FirebaseAnalytics.Event.Login, bundle);
            firebaseAnalytics.SetAnalyticsCollectionEnabled(true);

            //==========firebase End========================//

        }





        #region Button clicks

        private void Btn25_Click(object sender, EventArgs e)
        {

            if (checkInternet())
            {
                Common.WebUrl = "https://sites.google.com/view/pirithapp/videos";
                Intent intent = new Intent(this, typeof(webView));
                intent.AddFlags(ActivityFlags.NewTask);
                StartActivity(intent, ActivityOptions.MakeSceneTransitionAnimation(this).ToBundle());
                Finish();
            }
            else
            {
                showAlert(Resources.GetString(Resource.String.NoConnection));
            }

        }


        private void Btn29_Click(object sender, EventArgs e)
        {
            checkPlayback("IsigiliSutta.mp3", GetString(Resource.String.IsiGili));
            Playing.Text = btn29.Text + "\n" + GetString(Resource.String.Playing);
        }

        private void Btn28_Click(object sender, EventArgs e)
        {
            checkPlayback("AlawakaSutta.mp3", GetString(Resource.String.alawaka));
            Playing.Text = btn28.Text + "\n" + GetString(Resource.String.Playing);
        }

        private void Btn27_Click(object sender, EventArgs e)
        {
            checkPlayback("Girimaananda.mp3", GetString(Resource.String.girimananada));
            Playing.Text = btn27.Text + "\n" + GetString(Resource.String.Playing);
        }

        private void Btn26_Click(object sender, EventArgs e)
        {
            checkPlayback("MoraPiritha.mp3", GetString(Resource.String.mora_piritha));
            Playing.Text = btn26.Text + "\n" + GetString(Resource.String.Playing);
        }

        private void Btn23_Click(object sender, EventArgs e)
        {
            if (checkInternet())
            {
                Common.WebUrl = "https://www.thripitakaya.org/";
                Intent intent = new Intent(this, typeof(webView));

                StartActivity(intent, ActivityOptions.MakeSceneTransitionAnimation(this).ToBundle());
                Finish();
            }
            else
            {
                showAlert(GetString(Resource.String.NoConnection));
            }

        }

        private void Btn22_Click(object sender, EventArgs e)
        {
            checkPlayback("SarwaRathrikaParithanaDeshanawa.mp3", GetString(Resource.String.sarwa_rathrika));
            Playing.Text = btn22.Text + "\n" + GetString(Resource.String.Playing);
        }

        private void Btn21_Click(object sender, EventArgs e)
        {
            checkPlayback("Bodhi_Pooja_By_Hapugoda_Thero.mp3", GetString(Resource.String.bodhipuja));
            Playing.Text = btn21.Text + "\n" + GetString(Resource.String.Playing);
        }

        private void Btn20_Click(object sender, EventArgs e)
        {
            checkPlayback("AbhisambhidanaPiritha.mp3", GetString(Resource.String.AbhisambhidanaPiritha));
            Playing.Text = btn20.Text + "\n" + GetString(Resource.String.Playing);
        }

        private void Btn19_Click(object sender, EventArgs e)
        {
            checkPlayback("SathBuduWandanawa.mp3", GetString(Resource.String.SathBuduWandanawa));
            Playing.Text = btn19.Text + "\n" + GetString(Resource.String.Playing);
        }


        private void Btn18_Click(object sender, EventArgs e)
        {
            checkPlayback("rathana-sutta-one.mp3", GetString(Resource.String.rathanasuththa));
            Playing.Text = btn18.Text + "\n" + GetString(Resource.String.Playing);
        }


        private void Btn17_Click(object sender, EventArgs e)
        {
            checkPlayback("Kanda_Piritha.mp3", GetString(Resource.String.Kanda_Piritha));
            Playing.Text = btn17.Text + "\n" + GetString(Resource.String.Playing);
        }

        private void Btn16_Click(object sender, EventArgs e)
        {
            checkPlayback("bojjhanga_kv.mp3", GetString(Resource.String.bojjhanga));
            Playing.Text = btn16.Text + "\n" + GetString(Resource.String.Playing);
        }

        private void Btn15_Click(object sender, EventArgs e)
        {
            checkPlayback("Atavisi_Piritha.mp3", GetString(Resource.String.AtavisiPiritha));
            Playing.Text = btn15.Text + "\n" + GetString(Resource.String.Playing);
        }

        private void btnplay_Click(object sender, EventArgs e)
        {


            if (_player != null)
            {
                if (!_player.IsPlaying)
                {
                    btnplay.SetBackgroundResource(Resource.Drawable.baseline_pause_24);
                    _player.Start();

                }
                else if (_player.IsPlaying)
                {
                    btnplay.SetBackgroundResource(Resource.Drawable.baseline_play_arrow_24);
                    _player.Pause();
                }
            }
        }

        private void btnstop_Click(object sender, EventArgs e)
        {
            if (_player != null)
            {
                if (_player.IsPlaying)
                {
                    PlayerView.Visibility = ViewStates.Gone;
                    _player.Stop();
                }
                else
                {
                    PlayerView.Visibility = ViewStates.Gone;
                }
            }
        }


        private void Btn14_Click(object sender, EventArgs e)
        {
            checkPlayback("JathaManawaka_Gatha.mp3", GetString(Resource.String.JathaManawakaGatha));
            Playing.Text = btn14.Text + "\n" + GetString(Resource.String.Playing);

        }

        private void Btn13_Click(object sender, EventArgs e)
        {
            checkPlayback("JinapanjaraPiritha.mp3", GetString(Resource.String.JinapanjaraPirithaUrl));
            Playing.Text = btn13.Text + "\n" + GetString(Resource.String.Playing);
        }

        private void Btn12_Click(object sender, EventArgs e)
        {
            checkPlayback("WattakaPiritha.mp3", GetString(Resource.String.wattakkaPirithaUrl));
            Playing.Text = btn12.Text + "\n" + GetString(Resource.String.Playing);
        }

        private void Btn11_Click(object sender, EventArgs e)
        {
            checkPlayback("dasa_disa_piritha.mp3", GetString(Resource.String.dasaDisaPirithaUrl));
            Playing.Text = btn11.Text + "\n" + GetString(Resource.String.Playing);
        }

        private void Btn10_Click(object sender, EventArgs e)
        {
            checkPlayback("AnawumPiritha.mp3", GetString(Resource.String.anawumPirithaUrl));
            Playing.Text = btn10.Text + "\n" + GetString(Resource.String.Playing);
        }

        private void Btn9_Click(object sender, EventArgs e)
        {
            checkPlayback("AngulimaalaPiritha.mp3", GetString(Resource.String.angulimalaPirithaUrl));
            Playing.Text = btn9.Text + "\n" + GetString(Resource.String.Playing);
        }

        private void Btn8_Click(object sender, EventArgs e)
        {
            checkPlayback("RathaMaleeYanthra.mp3", GetString(Resource.String.rathnamaliPirithaUrl));
            Playing.Text = btn8.Text + "\n" + GetString(Resource.String.Playing);
        }

        private void Btn7_Click(object sender, EventArgs e)
        {
            checkPlayback("AntharayaNiwaranaPiritha.mp3", GetString(Resource.String.antharayaPirithaUrl));
            Playing.Text = btn7.Text + "\n" + GetString(Resource.String.Playing);
        }

        private void Btn6_Click(object sender, EventArgs e)
        {
            checkPlayback("JalaNandanaPiritha.mp3", GetString(Resource.String.jalanandanaPirithaUrl));
            Playing.Text = btn6.Text + "\n" + GetString(Resource.String.Playing);
        }

        private void Btn5_Click(object sender, EventArgs e)
        {
            checkPlayback("DajaghghaSutta.mp3", GetString(Resource.String.dajaggaPirithaUrl));
            Playing.Text = btn5.Text + "\n" + GetString(Resource.String.Playing);
        }

        private void Btn4_Click(object sender, EventArgs e)
        {
            checkPlayback("DhammaChakka1.mp3", GetString(Resource.String.dammachakkaPirithaUrl));
            Playing.Text = btn4.Text + "\n" + GetString(Resource.String.Playing);
        }

        private void Btn3_Click(object sender, EventArgs e)
        {

            checkPlayback("SeevaliePiritha.mp3", GetString(Resource.String.seewaliPirithaUrl));
            Playing.Text = btn3.Text + "\n" + GetString(Resource.String.Playing);
        }

        private void Btn2_Click(object sender, EventArgs e)
        {
            checkPlayback("JayaPiritha.mp3", GetString(Resource.String.jayaPirithaUrl));
            Playing.Text = btn2.Text + "\n" + GetString(Resource.String.Playing);
        }

        private void Btn1_Click(object sender, EventArgs e)
        {
            checkPlayback("MahaPiritha.mp3", GetString(Resource.String.mahaPirithaUrl));
            Playing.Text = btn1.Text + "\n" + GetString(Resource.String.Playing);
        }

        #endregion

        private void checkPlayback(string selected, string url)
        {
            try
            {
                selectedFile = selected;
                var filepath = Path.Combine(FolderPath, selected);
                if (System.IO.File.Exists(filepath))
                {

                    playbyfile(filepath);
                }
                else
                {
                    if (checkInternet())
                    {
                        DownloadFile(url, selectedFile);
                    }
                    else
                    {
                        showAlert(GetString(Resource.String.NoConnection) + "\n" + "\n" + GetString(Resource.String.FirstTimeMsg));
                    }

                }
            }
            catch (Exception)
            {

                return;
            }
            
        }


        private void Control_Click(object sender, EventArgs e)
        {
            stopPlayer();
        }


        private void showAlert(string alertmsg)
        {
            try
            {
                Android.App.AlertDialog.Builder dialog = new AlertDialog.Builder(this);
                AlertDialog alert = dialog.Create();
                alert.SetTitle("");
                alert.SetMessage(alertmsg);
                alert.SetButton(GetString(Resource.String.ExitMsg), (c, ev) =>
                {
                    // Ok button click task  
                });
                alert.Show();
            }
            catch (Exception)
            {

                return;
            }
            
        }

        private bool checkInternet()
        {
            try
            {
                var current = Connectivity.NetworkAccess;

                if (current == NetworkAccess.Internet)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {

                return false;
            }
           
        }

        private void showIndicator()
        {
            try
            {
                alert = new Android.App.AlertDialog.Builder(this);
                alert.SetView(Resource.Layout.progress);
                alert.SetCancelable(false);
                alert.SetPositiveButton(GetString(Resource.String.CancelMsg), (senderAlert, args) =>
                {
                    downloadManager.AbortAll();
                });

                //dialog = alert.Create();
            }
            catch (Exception)
            {

                return;
            }
            
        }


        private void playbyfile(string filepath)
        {
            try
            {
                if (_player != null)
                {
                    if (_player.IsPlaying)
                    {
                        _player.Stop();
                    }

                    var u = Android.Net.Uri.Parse(filepath);
                    _player = MediaPlayer.Create(this, u);
                    _player.Start();

                    PlayerView.Visibility = ViewStates.Visible;
                    btnplay.SetBackgroundResource(Resource.Drawable.baseline_pause_24);

                }
                else
                {
                    var u = Android.Net.Uri.Parse(filepath);
                    _player = MediaPlayer.Create(this, u);
                    _player.Start();

                    PlayerView.Visibility = ViewStates.Visible;
                    btnplay.SetBackgroundResource(Resource.Drawable.baseline_pause_24);
                }
            }
            catch (Exception ex)
            {
                return;
            }

        }

        private void DownloadFile(string url, string fileName)
        {
            try
            {

                if (!spaceavaliable())
                {
                    Android.App.AlertDialog.Builder dialog = new AlertDialog.Builder(this);
                    AlertDialog alert = dialog.Create();
                    alert.SetTitle("");
                    alert.SetMessage(GetString(Resource.String.NoSpaceMsg));
                    alert.SetIcon(Resource.Drawable.alert);
                    alert.SetButton(GetString(Resource.String.ExitMsg), (c, ev) =>
                    {

                    });
                    alert.Show();
                    return;
                }

                stopPlayer();
                PlayerView.Visibility = ViewStates.Gone;
                dialog = alert.Create();
                dialog.Show();

                downloadManager = CrossDownloadManager.Current;

                CrossDownloadManager.Current.PathNameForDownloadedFile = new System.Func<IDownloadFile, string>(file =>
                {

                    return Path.Combine(FolderPath, fileName);
                });


                var file2 = downloadManager.CreateDownloadFile(url);
                (CrossDownloadManager.Current as DownloadManagerImplementation).IsVisibleInDownloadsUi = false;
                //(CrossDownloadManager.Current as DownloadManagerImplementation).NotificationVisibility = DownloadVisibility.VisibleNotifyCompleted;

                downloadManager.Start(file2);
                downloadManager.CollectionChanged += DownloadManager_CollectionChanged;

            }
            catch (Exception)
            {

                return;
            }

        }

        private bool spaceavaliable()
        {
            try
            {
                Java.IO.File path = Android.OS.Environment.ExternalStorageDirectory;
                StatFs stat = new StatFs(path.AbsolutePath);
                long blockSize = stat.BlockSize;
                long availableBlocks = stat.AvailableBlocks;
                var a = Formatter.FormatShortFileSize(this, availableBlocks * blockSize);

                if (Convert.ToDouble((availableBlocks * blockSize)/100000000) < 2)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception)
            {

                return false;
            }
           

        }

        private void DownloadManager_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            
           
            try
            {
                dialog.Hide();
                downloadManager.CollectionChanged -= DownloadManager_CollectionChanged;

                var p = Path.Combine(FolderPath, selectedFile);
                if (System.IO.File.Exists(p))
                {
                    playbyfile(p);
                }
                selectedFile = "";


            }
            catch (Exception ex)
            {
                return;
            }
        }




        protected override void OnPause()
        {
            try
            {
                if (mAdView != null)
                {
                    mAdView.Pause();
                }
            }
            catch (Exception)
            {

                return;
            }
            base.OnPause();
        }

        protected override void OnResume()
        {
            try
            {
                if (mAdView != null)
                {
                    mAdView.Resume();
                }
            }
            catch (Exception)
            {

                return;
            }

            base.OnResume();
        }

        protected override void OnDestroy()
        {
            try
            {

                if (mAdView != null)
                {
                    mAdView.Destroy();
                }
            }
            catch (Exception)
            {

                return;
            }

            stopPlayer();

            base.OnDestroy();
        }

        class AdListener : Android.Gms.Ads.AdListener
        {
            MainActivity that;

            public AdListener(MainActivity t)
            {
                that = t;
            }

        }

        class OnClickListener : Java.Lang.Object, View.IOnClickListener
        {
            MainActivity that;

            public OnClickListener(MainActivity t)
            {
                that = t;
            }

            public void OnClick(View v)
            {
                if (that.mInterstitialAd.IsLoaded)
                {
                    that.mInterstitialAd.Show();
                }
                else
                {

                }
            }
        }


        public override void OnBackPressed()
        {

            try
            {
                DrawerLayout drawer = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
                if (drawer.IsDrawerOpen(GravityCompat.Start))
                {
                    drawer.CloseDrawer(GravityCompat.Start);
                }
                else
                {
                    Android.App.AlertDialog.Builder dialog = new AlertDialog.Builder(this);
                    AlertDialog alert = dialog.Create();
                    alert.SetTitle(GetString(Resource.String.ExitMsg));
                    alert.SetMessage(GetString(Resource.String.PermissionToExitMsg));
                    alert.SetIcon(Resource.Drawable.alert);
                    alert.SetButton(GetString(Resource.String.YesMsg), (c, ev) =>
                    {
                        stopPlayer();
                        this.Finish();
                        base.OnBackPressed();
                    });
                    alert.SetButton2(GetString(Resource.String.NoMsg), (c, ev) => { });
                    alert.Show();
                }
            }
            catch (Exception)
            {

                return;
            }
            
        }

        public void stopPlayer()
        {
            try
            {
                if (_player != null)
                {
                    if (_player.IsPlaying)
                    {
                        _player.Stop();
                    }
                }
                PlayerView.Visibility = ViewStates.Gone;
            }
            catch (Exception)
            {

                return;
            }

        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.menu_main, menu);
            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            
            int id = item.ItemId;
            if (id == Resource.Id.action_settings)
            {
                Android.App.AlertDialog.Builder dialog = new AlertDialog.Builder(this);
                AlertDialog alert = dialog.Create();
                alert.SetTitle(Resources.GetString(Resource.String.ExitMsg));
                alert.SetMessage(Resources.GetString(Resource.String.PermissionToExitMsg));
                alert.SetIcon(Resource.Drawable.alert);
                alert.SetButton(Resources.GetString(Resource.String.YesMsg), (c, ev) =>
                {
                    this.Finish();
                    base.OnBackPressed();
                });
                alert.SetButton2(Resources.GetString(Resource.String.NoMsg), (c, ev) => { });
                alert.Show();

                return true;
            }
            //else if (id == Resource.Id.action_feedback || id == Resource.Id.action_feedback_Icon)
            //{
            //    if (checkInternet())
            //    {
            //        Intent intent = new Intent(this, typeof(webView));
            //        Common.WebUrl = "https://sites.google.com/view/pirithapp/contact";
            //        StartActivity(intent, ActivityOptions.MakeSceneTransitionAnimation(this).ToBundle());
            //        Finish();
            //    }
            //    else
            //    {
            //        showAlert(Resources.GetString(Resource.String.NoConnection));
            //    }

            //    return true;
            //}

            //else if (id == Resource.Id.action_Help || id== Resource.Id.action_Help2)
            //{
            //    if (checkInternet())
            //    {
            //        Intent intent = new Intent(this, typeof(webView));
            //        Common.WebUrl = "https://sites.google.com/view/pirithapp/qa";
            //        StartActivity(intent, ActivityOptions.MakeSceneTransitionAnimation(this).ToBundle());
            //        Finish();
            //    }
            //    else
            //    {
            //        showAlert(Resources.GetString(Resource.String.NoConnection));
            //    }
                    

            //    return true;
            //}



            return base.OnOptionsItemSelected(item);
        }

        private void FabOnClick(object sender, EventArgs eventArgs)
        {
            stopPlayer();
        }

        public bool OnNavigationItemSelected(IMenuItem item)
        {
            int id = item.ItemId;

            if (!checkInternet())
            {
                showAlert(Resources.GetString(Resource.String.NoConnection));
                return false;
            }


            if (id == Resource.Id.nav_camera)
            {

                Android.Net.Uri uri = Android.Net.Uri.Parse("market://details?id=" + "lk.stechbuzz.qrreader");
                var openmarket = new Intent(Intent.ActionView, uri);
                StartActivity(openmarket);
            }

            else if (id == Resource.Id.nav_manage)
            {
                Android.Net.Uri uri = Android.Net.Uri.Parse("market://details?id=" + "lk.stechbuzz.electronic");
                var openmarket = new Intent(Intent.ActionView, uri);
                StartActivity(openmarket);
            }


            else if (id == Resource.Id.nav_share)
            {
                Intent sendIntent = new Intent();
                sendIntent.SetAction(Intent.ActionSend);
                sendIntent.PutExtra(Intent.ExtraText, "https://play.google.com/store/apps/details?id=lk.stechbuzz.pirith");
                sendIntent.SetType("text/plain");
                StartActivity(sendIntent);
            }
            else if (id == Resource.Id.nav_send)
            {
                var emailIntent = new Intent(Android.Content.Intent.ActionSend);
                emailIntent.PutExtra(Android.Content.Intent.ExtraEmail, new[] { "stechbuzz@gmail.com" });
                emailIntent.SetType("text/plain");
                StartActivity(Intent.CreateChooser(emailIntent, "Contact with us"));
            }

            //else if (id == Resource.Id.nav_FeedBack)
            //{
            //    //var Intent = new Intent(this, typeof(EmailActivity));
            //    //StartActivity(Intent);
            //    if (checkInternet())
            //    {
            //        Intent intent = new Intent(this, typeof(webView));
            //        Common.WebUrl = "https://sites.google.com/view/pirithapp/contact";
            //        StartActivity(intent, ActivityOptions.MakeSceneTransitionAnimation(this).ToBundle());
            //        Finish();
            //    }
            //    else
            //    {
            //        showAlert(Resources.GetString(Resource.String.NoConnection));
            //    }

            //}

            //else if (id == Resource.Id.nav_FAQ)
            //{
            //    Intent intent = new Intent(this, typeof(webView));
            //    Common.WebUrl = "https://sites.google.com/view/pirithapp/qa";
            //    StartActivity(intent, ActivityOptions.MakeSceneTransitionAnimation(this).ToBundle());
            //    Finish();
            //}



            DrawerLayout drawer = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            drawer.CloseDrawer(GravityCompat.Start);
            return true;
        }


//=====firebase check google play
        public void IsPlayServicesAvailable()
        {
            var resultCode = GoogleApiAvailability.Instance.IsGooglePlayServicesAvailable(this);
            if (resultCode == ConnectionResult.Success)
            {
                Log.Debug(TAG, "InstanceID token: " + FirebaseInstanceId.Instance.Token);
                FirebaseMessaging.Instance.SubscribeToTopic("PirithNews");
            }
            else
            {
                //if (GoogleApiAvailability.Instance.IsUserResolvableError(resultCode))
                //{
                //    msgText.Text = GoogleApiAvailability.Instance.GetErrorString(resultCode);
                //}
                //else
                //{
                //    msgText.Text = "This device is not supported";
                //}
            }

        }

//======firebase create notification channel
        void CreateNotificationChannel()
        {
            if (Build.VERSION.SdkInt < BuildVersionCodes.O)
            {
                return;
            }

            var channel = new NotificationChannel(CHANNEL_ID, Resources.GetString(Resource.String.app_name), NotificationImportance.Default)
            {
                Description = ""
            };

            var notificationManager = (NotificationManager)GetSystemService(NotificationService);
            notificationManager.CreateNotificationChannel(channel);
        }

//====permission request
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

    }
}

